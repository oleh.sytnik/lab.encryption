﻿using System.ComponentModel;
using System.Windows.Forms;

namespace def
{
    internal partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btClear = new System.Windows.Forms.Button();
            this.btKey = new System.Windows.Forms.Label();
            this.lbText = new System.Windows.Forms.Label();
            this.btHash = new System.Windows.Forms.Button();
            this.btFolder = new System.Windows.Forms.Button();
            this.btSign = new System.Windows.Forms.Button();
            this.btDecrypt = new System.Windows.Forms.Button();
            this.btEncrypt = new System.Windows.Forms.Button();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.tbOutput = new System.Windows.Forms.TextBox();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.tbText = new System.Windows.Forms.TextBox();
            this.FBD = new System.Windows.Forms.FolderBrowserDialog();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btClear
            // 
            this.btClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btClear.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btClear.Location = new System.Drawing.Point(325, 146);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(100, 40);
            this.btClear.TabIndex = 24;
            this.btClear.Text = "Clear console";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.EventClear);
            // 
            // btKey
            // 
            this.btKey.AutoSize = true;
            this.btKey.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btKey.Location = new System.Drawing.Point(12, 40);
            this.btKey.Name = "btKey";
            this.btKey.Size = new System.Drawing.Size(37, 19);
            this.btKey.TabIndex = 23;
            this.btKey.Text = "Key";
            // 
            // lbText
            // 
            this.lbText.AutoSize = true;
            this.lbText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbText.Location = new System.Drawing.Point(11, 9);
            this.lbText.Name = "lbText";
            this.lbText.Size = new System.Drawing.Size(39, 19);
            this.lbText.TabIndex = 22;
            this.lbText.Text = "Text";
            // 
            // btHash
            // 
            this.btHash.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btHash.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btHash.Location = new System.Drawing.Point(115, 146);
            this.btHash.Name = "btHash";
            this.btHash.Size = new System.Drawing.Size(100, 40);
            this.btHash.TabIndex = 21;
            this.btHash.Text = "Create hash";
            this.btHash.UseVisualStyleBackColor = true;
            this.btHash.Click += new System.EventHandler(this.EventGenHash);
            // 
            // btFolder
            // 
            this.btFolder.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btFolder.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btFolder.Location = new System.Drawing.Point(10, 146);
            this.btFolder.Name = "btFolder";
            this.btFolder.Size = new System.Drawing.Size(100, 40);
            this.btFolder.TabIndex = 20;
            this.btFolder.Text = "Open folder";
            this.btFolder.UseVisualStyleBackColor = true;
            this.btFolder.Click += new System.EventHandler(this.EventSelectFolder);
            // 
            // btSign
            // 
            this.btSign.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btSign.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btSign.Location = new System.Drawing.Point(220, 146);
            this.btSign.Name = "btSign";
            this.btSign.Size = new System.Drawing.Size(100, 40);
            this.btSign.TabIndex = 19;
            this.btSign.Text = "Sign a file";
            this.btSign.UseVisualStyleBackColor = true;
            this.btSign.Click += new System.EventHandler(this.EventSignFile);
            // 
            // btDecrypt
            // 
            this.btDecrypt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btDecrypt.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btDecrypt.Location = new System.Drawing.Point(336, 34);
            this.btDecrypt.Name = "btDecrypt";
            this.btDecrypt.Size = new System.Drawing.Size(95, 30);
            this.btDecrypt.TabIndex = 18;
            this.btDecrypt.Text = "Decrypt";
            this.btDecrypt.UseVisualStyleBackColor = true;
            this.btDecrypt.Click += new System.EventHandler(this.EventDecrypt);
            // 
            // btEncrypt
            // 
            this.btEncrypt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btEncrypt.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btEncrypt.Location = new System.Drawing.Point(239, 34);
            this.btEncrypt.Name = "btEncrypt";
            this.btEncrypt.Size = new System.Drawing.Size(95, 30);
            this.btEncrypt.TabIndex = 17;
            this.btEncrypt.Text = "Encrypt";
            this.btEncrypt.UseVisualStyleBackColor = true;
            this.btEncrypt.Click += new System.EventHandler(this.EventEncrypt);
            // 
            // tbConsole
            // 
            this.tbConsole.BackColor = System.Drawing.Color.White;
            this.tbConsole.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbConsole.Location = new System.Drawing.Point(6, 193);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.ReadOnly = true;
            this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbConsole.Size = new System.Drawing.Size(426, 91);
            this.tbConsole.TabIndex = 16;
            // 
            // tbOutput
            // 
            this.tbOutput.BackColor = System.Drawing.Color.White;
            this.tbOutput.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbOutput.Location = new System.Drawing.Point(4, 68);
            this.tbOutput.Multiline = true;
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.ReadOnly = true;
            this.tbOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbOutput.Size = new System.Drawing.Size(426, 72);
            this.tbOutput.TabIndex = 15;
            // 
            // tbKey
            // 
            this.tbKey.BackColor = System.Drawing.Color.White;
            this.tbKey.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbKey.Location = new System.Drawing.Point(56, 36);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(180, 26);
            this.tbKey.TabIndex = 14;
            // 
            // tbText
            // 
            this.tbText.BackColor = System.Drawing.Color.White;
            this.tbText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbText.Location = new System.Drawing.Point(56, 5);
            this.tbText.Name = "tbText";
            this.tbText.Size = new System.Drawing.Size(374, 26);
            this.tbText.TabIndex = 13;
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(435, 287);
            this.Controls.Add(this.btClear);
            this.Controls.Add(this.btKey);
            this.Controls.Add(this.lbText);
            this.Controls.Add(this.btHash);
            this.Controls.Add(this.btFolder);
            this.Controls.Add(this.btSign);
            this.Controls.Add(this.btDecrypt);
            this.Controls.Add(this.btEncrypt);
            this.Controls.Add(this.tbConsole);
            this.Controls.Add(this.tbOutput);
            this.Controls.Add(this.tbKey);
            this.Controls.Add(this.tbText);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Base";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "hashing sample";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btClear;
        private Label btKey;
        private Label lbText;
        private Button btHash;
        private Button btFolder;
        private Button btSign;
        private Button btDecrypt;
        private Button btEncrypt;
        private TextBox tbConsole;
        private TextBox tbOutput;
        private TextBox tbKey;
        private TextBox tbText;
        private FolderBrowserDialog FBD;
        private OpenFileDialog OFD;
    }
}