﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("winapi-proj")]
[assembly: AssemblyDescription("Sample of using WinAPI with C#")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("KHAI")]
[assembly: AssemblyProduct("winapi-proj")]
[assembly: AssemblyCopyright("KHAI")]
[assembly: AssemblyTrademark("KHAI")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(true)]
[assembly: Guid("a56a3272-6999-46bc-abe3-0a5512ed9c9e")]
[assembly: AssemblyVersion("1.0.2.0")]
[assembly: AssemblyFileVersion("1.0.2.0")]