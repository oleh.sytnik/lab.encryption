﻿using System;
using System.IO;
using System.Windows.Forms;

namespace def
{
    internal partial class Base : Form
    {
        private uint _length;

        internal Base()
        {
            InitializeComponent();
        }

        private void EventEncrypt(object sender, EventArgs e)
        {
            var c = new Crypt();
            var inp = tbText.Text;
            var key = tbKey.Text;
            var s = c.Get_Hash(inp, key, ref _length);
            tbOutput.Text = s;
        }

        private void EventDecrypt(object sender, EventArgs e)
        {
            var c = new Crypt();
            var key = tbKey.Text;
            var inp = tbOutput.Text;
            var s = c.DecryptFile(inp, key, ref _length);
            tbOutput.Text = s;
        }

        private void EventSignFile(object sender, EventArgs e)
        {
            var c = new Crypt();
            const string add = "\r\n```";
            if (OFD.ShowDialog() != DialogResult.OK) return;
            var sr = new StreamReader(OFD.FileName);
            var s = sr.ReadToEnd();
            sr.Close();
            if (s.IndexOf(add, StringComparison.Ordinal) != -1)
            {
                var oldHash = s.Remove(0, s.IndexOf(add, StringComparison.Ordinal) + add.Length);
                var text = s.Substring(0, s.IndexOf(add, StringComparison.Ordinal));
                var hash2 = c.Sign(text);

                if (VerifyHash(oldHash, hash2))
                {
                    MessageBox.Show(@"Signature created successfully");
                }
                else
                {
                    if (MessageBox.Show(@"Rewrite signature?", @"Rewrite", MessageBoxButtons.YesNo) ==
                        DialogResult.Yes)
                    {
                        using (var outStream = new StreamWriter(OFD.FileName))
                        {
                            outStream.Write(text + add + hash2);
                            MessageBox.Show(@"Signature created successfully! " + hash2);
                        }
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            else
            {
                var hash1 = c.Sign(s);
                using (var outStream = new StreamWriter(OFD.FileName))
                {
                    outStream.Write(s + add + hash1);
                    MessageBox.Show(@"Signature created successfully! " + hash1);
                }
            }
        }

        public bool VerifyHash(string hashOfInput, string hash)
        {
            var comparer = StringComparer.OrdinalIgnoreCase;
            return 0 == comparer.Compare(hashOfInput, hash);
        }

        public string[] Files;

        private void EventSelectFolder(object sender, EventArgs e)
        {
            if (FBD.ShowDialog() != DialogResult.OK) return;
            Files = Directory.GetFiles(FBD.SelectedPath);
            MessageBox.Show(@"Files found: " + Files.Length);
        }

        private void EventGenHash(object sender, EventArgs e)
        {
            var c = new Crypt();
            if (Files == null)
            {
                MessageBox.Show(@"Choose a folder for hashing!");
                return;
            }
            for (var i = 0; i < Files.Length; i++)
            {
                var br = File.ReadAllText(Files[i]);
                var hash1 = c.Hash(br);
                tbConsole.Text += @"Hash " + i + @" = " + hash1;
                tbConsole.Text += @"
";
            }
        }

        private void EventClear(object sender, EventArgs e)
        {
            tbConsole.Text = "";
        }
    }
}