﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace def
{
    public class Crypt
    {
        // ReSharper disable once InconsistentNaming
        public const uint PROV_RSA_FULL = 1;

        // ReSharper disable once InconsistentNaming
        public const uint CRYPT_VERIFYCONTEXT = 0xF0000000;

        // ReSharper disable once InconsistentNaming
        public const uint CRYPT_NEWKEYSET = 0x00000008;

        // ReSharper disable once InconsistentNaming
        private const int HP_HASHVAL = 0x0002;

        // ReSharper disable once InconsistentNaming
        public enum ALG_ID
        {
            // ReSharper disable once InconsistentNaming
            CALG_MD5 = 0x00008003,

            // ReSharper disable once InconsistentNaming
            CALG_RC2 = 26114,

            // ReSharper disable once InconsistentNaming
            AT_SIGNATURE = 2
        }

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptAcquireContext(out IntPtr phProv, string pszContainer, string pszProvider,
            uint dwProvType, uint dwFlags);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptCreateHash(IntPtr hProv, ALG_ID Algid, IntPtr hKey, uint dwFlags,
            out IntPtr phHash);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptHashData(IntPtr hHash, byte[] pbData, int dwDataLen, uint dwFlags);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDeriveKey(IntPtr hProv, ALG_ID Algid, IntPtr hBaseData, uint dwFlags,
            out IntPtr phKey);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDestroyHash(IntPtr hHash);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDecrypt(IntPtr hKey, IntPtr hHash, [MarshalAs(UnmanagedType.Bool)] bool Final,
            uint dwFlags, byte[] pbData, ref uint pdwDataLen);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDestroyKey(IntPtr hKey);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptReleaseContext(IntPtr hProv, uint dwFlags);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptEncrypt(IntPtr hKey, IntPtr hHash, int Final, uint dwFlags, byte[] pbData,
            out uint pdwDataLen, uint dwBufLen);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptGetHashParam(IntPtr hHash, int dwParam, byte[] pbData, ref int pdwDataLen,
            int dwFlags);

        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptGenKey(IntPtr hProv, ALG_ID Algid, uint dwFlags, out IntPtr phKey);

        private const string MS_DEF_PROV = "Microsoft Base Cryptographic Provider v1.0";
        private const uint NTE_BAD_KEYSET = 0x80090016;

        public string Get_Hash(string str, string key, ref uint DataLen)
        {
            IntPtr hKey;
            IntPtr hHash;
            IntPtr hCryptProv;
            if (!CryptAcquireContext(out hCryptProv, null, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
            {
                if (Marshal.GetLastWin32Error() == NTE_BAD_KEYSET)
                {
                    if (!CryptAcquireContext(out hCryptProv, null, null, PROV_RSA_FULL, CRYPT_NEWKEYSET))
                        return "";
                }
                else
                    return "";
            }
            if (!CryptCreateHash(hCryptProv, ALG_ID.CALG_MD5, IntPtr.Zero, 0, out hHash))
                return "";
            var bkey = GetBytes(key);
            if (!CryptHashData(hHash, bkey, bkey.Length, 0))
                return "";
            if (!CryptDeriveKey(hCryptProv, ALG_ID.CALG_RC2, hHash, 0, out hKey))
                return "";
            if (!CryptDestroyHash(hHash))
                return "";
            DataLen = Convert.ToUInt32(str.Length);
            var dataLen = Convert.ToUInt32(str.Length);
            if (!CryptEncrypt(hKey, IntPtr.Zero, 1, 0, null, out dataLen, 0))
                return "";
            for (var i = DataLen; i < dataLen; i++)
            {
                str += "0";
            }
            var bstr = Encoding.Default.GetBytes(str);
            if (!CryptEncrypt(hKey, IntPtr.Zero, 1, 0, bstr, out DataLen, dataLen))
                return "";
            if (hKey != IntPtr.Zero)
            {
                if (!CryptDestroyKey(hKey))
                    return "";
            }
            if (hCryptProv != IntPtr.Zero)
            {
                if (!CryptReleaseContext(hCryptProv, 0))
                    return "";
            }
            var res = Encoding.Default.GetString(bstr);
            return res;
        }

        public byte[] GetBytes(string str)
        {
            var encoding = new ASCIIEncoding();
            var bytes = encoding.GetBytes(str);
            return bytes;
        }

        public string DecryptFile(string str, string key, ref uint dataLen)
        {
            IntPtr hKey;
            IntPtr hHash;
            IntPtr hCryptProv;
            if (!CryptAcquireContext(out hCryptProv, null, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
            {
                if (Marshal.GetLastWin32Error() == NTE_BAD_KEYSET)
                {
                    if (!CryptAcquireContext(out hCryptProv, null, null, PROV_RSA_FULL, CRYPT_NEWKEYSET))
                        return "";
                }
                else
                    return "";
            }
            if (!CryptCreateHash(hCryptProv, ALG_ID.CALG_MD5, IntPtr.Zero, 0, out hHash))
                return "";
            var bkey = GetBytes(key);
            if (!CryptHashData(hHash, bkey, bkey.Length, 0))
                return "";
            if (!CryptDeriveKey(hCryptProv, ALG_ID.CALG_RC2, hHash, 0, out hKey))
                return "";
            if (!CryptDestroyHash(hHash))
                return "";
            var bstr = Encoding.Default.GetBytes(str);
            if (!CryptDecrypt(hKey, IntPtr.Zero, true, 0, bstr, ref dataLen))
                return "";
            if (hKey != IntPtr.Zero)
            {
                if (!CryptDestroyKey(hKey))
                    return "";
            }
            if (hCryptProv != IntPtr.Zero)
            {
                if (!CryptReleaseContext(hCryptProv, 0))
                    return "";
            }
            var res = Encoding.Default.GetString(bstr);
            res = res.Substring(0, (int) dataLen);
            return res;
        }

        public string Sign(string str)
        {
            IntPtr hCryptProv;
            IntPtr hHash;
            if (!CryptAcquireContext(out hCryptProv, null, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
            {
                if (Marshal.GetLastWin32Error() == NTE_BAD_KEYSET)
                {
                    if (!CryptAcquireContext(out hCryptProv, null, null, PROV_RSA_FULL, CRYPT_NEWKEYSET))
                        return "";
                }
                else
                    return "";
            }
            if (!CryptCreateHash(hCryptProv, ALG_ID.CALG_MD5, IntPtr.Zero, 0, out hHash))
                return "";
            var bstr = GetBytes(str);
            if (!CryptHashData(hHash, bstr, bstr.Length, 0))
                return "";
            var len = bstr.Length;
            if (len < 16)
            {
                for (var i = len; i < 16; i++)
                {
                    str += "0";
                }
                len = str.Length;
                bstr = GetBytes(str);
            }
            if (!CryptGetHashParam(hHash, HP_HASHVAL, bstr, ref len, 0))
            {
                return "";
            }
            if (!CryptDestroyHash(hHash))
                return "";
            if (hCryptProv != IntPtr.Zero)
            {
                if (!CryptReleaseContext(hCryptProv, 0))
                    return "";
            }
            var res = Encoding.Default.GetString(bstr);
            res = res.Substring(0, len - 1);
            return res;
        }

        public string Hash(string str)
        {
            IntPtr hHash;
            IntPtr hCryptProv;
            if (!CryptAcquireContext(out hCryptProv, null, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
            {
                if (Marshal.GetLastWin32Error() == NTE_BAD_KEYSET)
                {
                    if (!CryptAcquireContext(out hCryptProv, null, null, PROV_RSA_FULL, CRYPT_NEWKEYSET))
                        return "";
                }
                else
                    return "";
            }
            if (!CryptCreateHash(hCryptProv, ALG_ID.CALG_MD5, IntPtr.Zero, 0, out hHash))
                return "";
            var bstr = GetBytes(str);

            if (!CryptHashData(hHash, bstr, bstr.Length, 0))
                return "";
            var len = bstr.Length;
            if (len < 16)
            {
                for (var i = len; i < 16; i++)
                {
                    str += "0";
                }
                len = str.Length;
                bstr = GetBytes(str);
            }
            string res;
            if (CryptGetHashParam(hHash, HP_HASHVAL, bstr, ref len, 0))
            {
                res = Encoding.Default.GetString(bstr);
                res = res.Substring(0, len);
            }
            else
            {
                return "";
            }
            return res;
        }
    }
}